import { Pipe, PipeTransform } from '@angular/core';
import { parsePhoneNumberFromString } from 'libphonenumber-js'

@Pipe({
  name: 'phone'
})
export class PhonePipe implements PipeTransform {

  transform(phone: string): any {
    try {
      return parsePhoneNumberFromString(phone, 'CO').formatNational();
    } catch (error) {
      return phone;
    }
  }

}
