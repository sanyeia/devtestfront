import { Routes, RouterModule } from "@angular/router";

import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { PagesComponent } from './pages/pages.component';
import { RegisterComponent } from './login/register/register.component';
import { LoginGuard } from './services/guards/login.guard';
import { CustomersComponent } from './pages/customers/customers.component';
import { SaleComponent } from './pages/sale/sale.component';
import { CustomerformComponent } from './pages/customers/customerform/customerform.component';
import { SaleformComponent } from './pages/sale/saleform/saleform.component';


const appRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [ LoginGuard ],
    children: [
      { path: 'dashboard', component: DashboardComponent },

      { path: 'customers', component: CustomersComponent },
      { path: 'customers/new', component: CustomerformComponent },
      { path: 'customers/:id', component: CustomerformComponent },

      { path: 'sales', component: SaleComponent },
      { path: 'sales/new', component: SaleformComponent },
      { path: 'sales/:id', component: SaleformComponent },

      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    ]
  },

  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', component: NopagefoundComponent },
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes, { useHash: true} );
