import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  tgglSearch:boolean = false;
  public options:Array<any> = [];

  constructor(
    public _uS: UserService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
  }

  logout(){
    this._uS.logout();
    this.router.navigate(['/login']);
  }
}
