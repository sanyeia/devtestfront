export interface User {
  email: string;
  phone: string;
  firstname: string;
  lastname: string;
  password: string;
  id?: string;
  created_at?: string;
  updated_at?: string;
}

export interface Customer {
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
  addresses?: Array<Address>;
  id?: string;
  created_at?: string;
  updated_at?: string;
}

export interface Address {
  address: string;
  country: string;
  state: string;
  city: string;
  description?: string;
  customer_id?: string;
  created_at?: string;
  updated_at?: string;
  id?: string;
}

export interface Invoice {
  status: number;
  notes?: string;

  customer?: Customer;
  products?: Array<Product>;

  address_id?: string;
  total?: number;
  id?: string;
  user_id?: string;
  customer_id?: string;
  created_at?: string;
  updated_at?: string;
}

export interface Product {
  name: string;
  cost: number;
  description: string;

  details?: {
    amount: number;
    invoice_id?: string;
    product_id?: string;
  };

  id?: string;
  created_at?: string;
  updated_at?: string;
}

export interface InvoiceProduct {
  product_id: string;
  amount: number;

  name?: string;
  price?: number;
}
