import {
  EventEmitter
} from '@angular/core';
import {
  Storage
} from './storage';

import * as _ from 'underscore';

/**
 * @ignore
 */
class Link {
  /**
   * @ignore
   */
  constructor(
    public spr: any,
    public page: number,
    public text: string,
    public current: boolean = false
  ) {}
  /**
   * @ignore
   */
  send() {
    this.spr.params = _.extend(this.spr.params, {});
    this.spr.send(this.page, this.spr.serializate(this.spr.params))
      .then((e) => this.spr.load_data(e));
  }
}

/**
 * Genera array paginado desde elementos de un API.
 */
export class Pagination {

  /**
   * Array con los items de la paginaciónn.
   * @type {any[]}
   */
  public items: any[] = [];
  /**
   * Array con los items seleccionados de la paginaciónn.
   * @type {any[]}
   */
  public itemsSelected: any[] = [];
  /**
   * Indica si esta en espera el componente.
   * @type {boolean}
   */
  public loading: boolean = false;
  /**
   * Tipo de ordenamiento de encabezados.
   * @type {string}
   */
  public ordertype: string = '';
  /**
   * Indice de ordenamiento.
   * @type {number}
   */
  public order_index: number;
  /**
   * Numero de Página actual.
   * @type {number}
   */
  public current_page: number = 0;
  /**
   * Numero total de items.
   * @type {number}
   */
  public total: number = 0;
  /**
   * Numero de item inical de la página actual.
   * @type {number}
   */
  public to: number = 0;
  /**
   * Numero de item final de la página actual.
   * @type {number}
   */
  public from: number = 0;
  /**
   * Numero de Páginas.
   * @type {number}
   */
  public pages: number = 0;
  /**
   * Etiqueta con la información de la paginación.
   * @type {string}
   */
  public label: string = '';
  /**
   * Array de items con los Links de las páginas adyacentes
   * @type {Link[]}
   */
  public links: Link[] = [];
  /**
   * @ignore
   */
  public strkey: string;
  /**
   * @ignore
   */
  public params: any = {};
  /**
   * @ignore
   */
  private source;
  /**
   * Evento que se ejecuta despues de crear la paginacion.
   */
  public afterload: EventEmitter < any[] > = new EventEmitter();
  /**
   * Evento que se ejecuta despues de crear la paginacion
   */
  public changeValue: EventEmitter < Object > = new EventEmitter();
  /**
   * Evento que se ejecuta despues de crear la paginacion cuando se genera un erro.
   */
  public hasError: EventEmitter < any > = new EventEmitter();

  /**
   * @param {string} url Ruta APIResfull del recurso a paginar.
   * @param {Array<any>} titles Array de titulos para la paginación.
   * @param {string} token string con la cadena del token access requerido por el API.
   * @param {number} per_page Numero maximo de items por página.
   * @param {any} prm Objeto con los Paramatros para incluir en la ruta de la petición.
   */
  constructor(
    public url ? : string,
    public titles ? : Array < any > ,
    public token ? : string,
    public per_page ? : number,
    prm ? : any,
    public filter ? : any
  ) {
    this.params = prm;
    let params = (prm) ? this.serializate(prm) : '';
    url && token ? this.send(1, params).then((e) => this.load_data(e)) : false;
  }

  /**
   * @ignore
   */
  load_data(e: any) {
    this.items = this.filter ? e.data.filter(item => this.filter(item)) : e.data;
    this.current_page = e.current_page;
    this.per_page = e.per_page
    this.total = e.total;
    this.to = e.to;
    this.from = e.from;
    this.pages = Math.ceil(e.total / e.per_page);

    if (this.from && this.to && this.total) {
      this.label = `Showing records ${ this.from } - ${ this.to } of ${ this.total }`;
    } else {
      this.label = '';
    }
    this.generate_links();
  }

  /**
   * Genera los links de las multiples páginas.
   */
  generate_links(): void {
    this.links = [];
    if (this.pages > 1) {
      let last = this.pages;
      let current = this.current_page;
      let wd = (last - current);
      let max = (current > 2) ? 3 : 6 - current;
      let min = (wd < 3) ? (4 - wd) : (5 - max);
      if ((current - min) > 1 && (current > 3)) {
        this.links.push(new Link(this, 1, 'First', true));
      }
      for (let i = (current - min); i < (current + max); i++) {
        if (i > 0 && i < (last + 1)) {
          this.links.push(new Link(this, i, `${i}`, i == this.current_page));
        }
      }
      if (wd > 2 && current > 5) {
        this.links.push(new Link(this, last, 'Last', last == this.current_page));
      }
    }
  }

  /**
   * @param {number} i Numero de
   */
  order(i: number) {
    if (!this.titles[i]) return false;
    let name_input = this.titles[i].name_input;
    if (name_input) {
      this.order_index = i;

      this.rmParam('sort_by');
      this.rmParam('sort_by_desc');

      let aux_params = Object.assign({}, this.params);

      switch (this.ordertype) {
        case '':
          this.ordertype = 'down';
          aux_params.sort_by = name_input;

          break;
        case 'down':
          this.ordertype = 'up';
          aux_params.sort_by_desc = name_input;
          break;
        case 'up':
          this.ordertype = 'down';
          aux_params.sort_by = name_input;
          break;
      }
      this.params = _.extend(this.params || {}, aux_params);
      this.refresh();
    } else {
      return false; // no tiene name_input
    }
  }

  /**
   * Refresca la informacion de la paginacion con el API.
   * @return No retorna información.
   */
  refresh(): void {
    //console.log('refresh', this.params);
    this.send(this.current_page, this.serializate(this.params)).then((e) => this.load_data(e));
  }

  /**
   * Envia parametros de búsqueda al API
   * @param {any} paras Objecto con los parametros de búsqueda.
   */
  find(params: any) {
    this.params = _.extend(this.params || {}, params);
    this.send(1, this.serializate(this.params))
      .then((e) => this.load_data(e));
  }

  paramsRemove(key: string) {
    if (this.params && this.params.hasOwnProperty(key)) {
      delete this.params[key];
    }
  }

  getTableBody() {
    return _.filter(this.items, (i) => {
      return !Array.isArray(i);
    });
  }


  /**
   * Carga los datos del API
   * @param {number = 1} page Numero de página a cargar;
   * @param {string = ''} search cadena con parametros url para la búsqueda en el API.
   */
  private send(page: number = 1, search: string = '') {
    //console.log('search', search);
    return new Promise((resolve, reject) => {
      let pg = `?page=${page}`;
      let request = new XMLHttpRequest();
      this.loading = true;

      this.changeValue.emit(false);
      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          if (request.status === 200) {
            this.changeValue.emit(false);
            resolve(JSON.parse(request.response));
            this.afterload.emit(JSON.parse(request.response));
          } else {
            this.hasError.emit(request.response);
            reject(JSON.parse(request.response));
            this.afterload.emit(JSON.parse(request.response));
          }
          this.loading = false;
        }
      };

      let url = this.url + pg + search;
      if (this.per_page)
        url += '&per_page=' + this.per_page;
      request.open('get', url, true);
      request.setRequestHeader('Accept', 'application/json');
      request.setRequestHeader('Authorization', `Bearer ${this.token}`);
      request.send();
    });
  }

  /**
   * Convierte Objeto a parametros url.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  serializate(obj: any) {
    if (!obj) return '';
    var output_string = [];
    Object.keys(obj).forEach((val) => {
      if (Array.isArray(obj[val])) {
        if (obj[val].length > 0) {
          for (let i of obj[val]) {
            if (this.noNull(i)) output_string.push(`${val}[]=${i}`);
          }
        }
      } else {
        let value = String(obj[val]);
        if (value.length > 0) {
          if (this.noNull(value)) output_string.push(`${val}=${value}`);
        }
      }
    });

    let results = output_string.join('&');
    if (results.length > 0) {
      return '&' + results;
    } else {
      return '';
    }
  }

  noNull(val) {
    let sw1 = val != null;
    let sw2 = typeof val !== undefined;
    let sw3 = val != 'null';
    return sw1 && sw2 && sw3;
  }

  /**
   * Agrega o elimina un objeto al array de items seleccionados.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  select(obj: any, $event ? : Event) {
    $event.stopPropagation();
    let index = this.itemsSelected.indexOf(obj);
    index != -1 ? this.itemsSelected.splice(index, 1) : this.itemsSelected.push(obj);
  }

  /**
   * Agrega o elimina un objeto al array de items seleccionados.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  selectOne(obj: any, $event ? : Event) {
    $event.stopPropagation();
    obj == this.itemsSelected[0] ? this.itemsSelected.splice(0, 1) : this.itemsSelected.splice(0, 1, obj);
  }

  /**
   * Selecciona o elimina todos los items.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  selectAll() {
    this.itemsSelected.length == this.items.length ? this.itemsSelected = [] : this.itemsSelected = this.items.slice(0);
  }

  /**
   * Retorna true si esta seleccionado.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  isChecked(obj: any) {
    return this.itemsSelected.indexOf(obj) != -1;
  }

  /**
   * Retorna true si esta seleccionado.
   * @param {any} obj Objecto a convertir en paramtros url.
   */
  isAllChecked() {
    return this.itemsSelected.length == this.items.length;
  }

  private rmParam(param) {
    if (this.params && this.params[param]) {
      delete this.params[param];
    };
  }

}
