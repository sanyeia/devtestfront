import { Component, OnInit } from '@angular/core';
import { Pagination } from '../../core/pagination';
import { SalesService } from '../../services/sales.service';
import { NotificationsService } from '../../services/notifications.service';
import { Invoice, Customer } from '../../interfaces/interfaces';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {


  public invoices:Pagination;
  public customers:Array<Customer>;

  public titles = [
    { name: "Cliente", name_input: "", pos: "left" },
    { name: "Fecha de Creacion", name_input: "created_at", pos: "left" },
    { name: "Total", name_input: "total", pos: "left" },
    { name: "Estatus", name_input: "status", pos: "center" },
  ];

  constructor(
    public _sS: SalesService,
    public _cS: CustomerService,
    public _nS: NotificationsService,
  ) {
    this.invoices = new Pagination(_sS.url.list, this.titles, _sS.getToken().toString(), 15, {});
    this._cS.list(true).subscribe( (resp:any) => {
      this.customers = resp.data;
    });
  }

  ngOnInit() {
  }

  filterCustomer(customer_id){
    if(customer_id != 'none'){
      this.invoices.find({customer_id: customer_id});
    }else{
      this.invoices.paramsRemove('customer_id');
      this.invoices.refresh();
    }
  }

  changeStatus(status, id:string){
    let invoice: Invoice = {
      status
    };
    console.log(invoice);
    this._sS.update(id, invoice).subscribe( (resp:any) => {
      this._nS.show('Estatus cambiado', resp.message, 'success');
      this.invoices.refresh();
    });
  }

  deleteinvoice(event, inv:Invoice){
    this._sS.remove(inv.id).subscribe( resp => {
      this._nS.show('Factura eliminada', 'La Factura se ha eliminado con exito', 'success');
      this.invoices.refresh();
    });
  }

}
