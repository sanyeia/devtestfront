import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Invoice, Product, InvoiceProduct, Customer, Address } from '../../../interfaces/interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../services/notifications.service';
import { CustomerService } from '../../../services/customer.service';
import { SalesService } from '../../../services/sales.service';
import { ProductService } from '../../../services/products.service';

@Component({
  selector: 'app-saleform',
  templateUrl: './saleform.component.html',
  styleUrls: ['./saleform.component.css']
})
export class SaleformComponent implements OnInit {

  id: string;
  invoiceForm: FormGroup;
  invoice: Invoice;
  valid: boolean;

  //producto seleccionado para agregar a la factura
  selectedProd: string;
  amountProd: number;

  //listado de opciones de factura
  customerList: Array<Customer> = [];
  addressesList: Array<Address> = [];
  productList: Array<Product> = [];

  //productos agregados a la factura
  products: Array<InvoiceProduct> = [];
  totalInvoice: number;

  public customerAttributes:any = {
    customer: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    status: new FormControl(''),
    notes: new FormControl(''),
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _cS: CustomerService,
    private _sS: SalesService,
    private _pS: ProductService,
    private _nS: NotificationsService,
  ) {
    this.invoiceForm = new FormGroup(this.customerAttributes);

    this.valid = true;
    this.selectedProd = 'none';
    this.amountProd = 0;
    this.totalInvoice = 0;

    //se obtiene el id de la ruta
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.initList();
      //se busca el id de la factura
      if(this.id){
        this.loadData();
      }else{
        this.setFields(true);
      }
    });
  }

  ngOnInit() {

  }

  initList(){
    //carga lista de clientes
    this._cS.list(true).subscribe( (resp:any) => {
      this.customerList = resp.data;
    });

    //carga lista de productos
    this._pS.list(true).subscribe( (resp:any) => {
      this.productList = resp.data;
    });

    this.invoiceForm.controls['address'].disable();
  }

  loadData(){
    if(!this.id){ return false; }

    //busca el id de la factura
    this._sS.show(this.id).subscribe( (resp:any) => {
      //informacion de la factura
      this.invoice = resp.data;

      //listado de productos en la factura
      this.invoice.products.forEach( (prod:any) => {
        this.products.push({
          product_id: prod.id,
          amount: prod.details.amount,
          name: prod.name,
          price: prod.cost,
        });
      });
      this.updateTotal();

      //carga la info del cliente
      this.addressesList = resp.data.customer.addresses;

      this.setFields();
    });
    return true;
  }

  setFields(toDefault:boolean = false){
    if(toDefault){
      this.invoiceForm.patchValue({
        customer: 'none',
        address: 'none',
        status: 1
     });
    }else{
      this.invoiceForm.patchValue({
        customer: this.invoice.customer_id,
        address: this.invoice.address_id,
        status: this.invoice.status,
        notes: this.invoice.notes,
     });
     this.invoiceForm.disable()
    }
  }

  //invoice

  saveInvoice(){
    if(this.invoiceForm.invalid){
      this._nS.show('Faltan Campos', 'Tiene que llenar todos los campos primero', 'error');
      this.valid = false;
      return;
    }else if( this.products.length <= 0 ){
      this._nS.show('Factura invalida', 'Debes tener al menos un producto', 'error');
    }else{
      let data: Invoice = {
        status: this.invoiceForm.get('status').value,
        notes: this.invoiceForm.get('notes').value,
      };

      this._sS.create(this.invoiceForm.get('address').value, data, this.products).subscribe( (resp:any) => {
        this._nS.show('Venta realizada', resp.message, 'success');
        this.router.navigate(['/sales']);
      });
    }
  }


  //customer
  updateAddresses(customer_id){
    console.log(customer_id);
    this._cS.show(customer_id).subscribe( (resp:any) => {
      this.addressesList = resp.data.addresses
      this.invoiceForm.controls.address.enable();
    });
  }

  //products

  addProduct(){
    //se comprueba que el registro de producto - cantidad sea valido
    if(this.selectedProd == 'none'){
      this._nS.show('Producto Invalido', 'Debe seleccionar un Producto primero', 'error');
    } else if( this.amountProd <= 0 ) {
      this._nS.show('Cantidad Invalida', 'La catidad de un producto tiene que ser mayor que 0', 'error');
    }else{
      //se agrega a la lista
      console.log(this.productList);
      console.log(this.selectedProd);
      let product = this.productList.find(prod => prod.id == this.selectedProd);
      this.products.push({
        product_id: product.id,
        amount: this.amountProd,
        name: product.name,
        price: product.cost,
      });
      this.updateTotal();
    }
  }

  removeProduct(event, invprod:InvoiceProduct){
    this.products = this.products.filter( obj => obj.product_id !== invprod.product_id );
    this.updateTotal();
  }

  updateTotal(){
    let total:number = 0;
    this.products.forEach( (prod:InvoiceProduct) => total += prod.price * prod.amount);
    this.totalInvoice = total;
  }
}
