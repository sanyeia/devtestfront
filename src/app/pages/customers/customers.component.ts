import { Component, OnInit } from '@angular/core';
import { Pagination } from '../../core/pagination';
import { CustomerService } from '../../services/customer.service';
import { Customer } from '../../interfaces/interfaces';
import { NotificationsService } from '../../services/notifications.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  public customers:Pagination;

  public titles = [
    { name: "Nombre", name_input: "firstname", pos: "left" },
    { name: "Apellido", name_input: "lastname", pos: "left" },
    { name: "Telefono", name_input: "phone", pos: "left" },
    { name: "Email", name_input: "email", pos: "left" },
    { name: "Fecha de Creacion", name_input: "created_at", pos: "left" },
  ];

  constructor(
    public _cS: CustomerService,
    public _nS: NotificationsService,
  ) {
    this.customers = new Pagination(_cS.url.main, this.titles, _cS.getToken().toString(), 15, {});
  }

  ngOnInit() {
  }

  deleteCustomer(event, cus:Customer){
    this._cS.remove(cus.id).subscribe( resp => {
      this._nS.show('Cliente eliminado', 'El Cliente se ha eliminado con exito', 'success');
      this.customers.refresh();
    });
  }
}
