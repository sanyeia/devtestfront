import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Address } from '../../../interfaces/interfaces';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from '../../../services/customer.service';
import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-addressform',
  templateUrl: './addressform.component.html',
  styleUrls: ['./addressform.component.css']
})
export class AddressformComponent implements OnInit {

  AddressForm: FormGroup;
  address: Address;
  id: string;

  public addressAttributes:any = {
    address: new FormControl('', Validators.required),
    description: new FormControl(''),
    country: new FormControl(''),
    state: new FormControl(''),
    city: new FormControl(''),
  };

  constructor(
    public addressModal: MatDialogRef<AddressformComponent>,
    public _cS: CustomerService,
    private _nS: NotificationsService,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit() {
    this.AddressForm = new FormGroup(this.addressAttributes);
    this.id = this.data.id;
    if(this.data.address){
      this.address = this.data.address;
      this.setFields();
    }
  }

  setFields(){
    this.AddressForm.patchValue({
      address: this.address.address,
      description: this.address.description,
      country: this.address.country,
      state: this.address.state,
      city: this.address.city,
   });
  }

  onCancel(){
    this.addressModal.close();
  }

  onCreate(){
    if(this.AddressForm.invalid){return}

    let address: Address = {
      address: this.AddressForm.get('address').value,
      description: this.AddressForm.get('description').value,
      country: this.AddressForm.get('country').value,
      state: this.AddressForm.get('state').value,
      city: this.AddressForm.get('city').value,
    };
    if(this.id){
      this._cS.createAddr(this.id, address).subscribe( (resp:any) => {
        this._nS.show('Direccion Creada', resp.message, 'success');
        this.addressModal.close();
      });
    }else{
      this.addressModal.close(address);
    }
  }

  onUpdate(){
    if(this.AddressForm.invalid){return}

    let address: Address = {
      address: this.AddressForm.get('address').value,
      description: this.AddressForm.get('description').value,
      country: this.AddressForm.get('country').value,
      state: this.AddressForm.get('state').value,
      city: this.AddressForm.get('city').value,
    };
    this._cS.updateAddr(this.address.id, address).subscribe( (resp:any) => {
      this._nS.show('Direccion Actualizada', resp.message, 'success');
      this.addressModal.close();
    });
  }

}
