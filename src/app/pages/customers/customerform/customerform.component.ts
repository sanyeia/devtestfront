import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Customer, Address } from '../../../interfaces/interfaces';
import { MatDialog } from '@angular/material';
import { CustomerService } from '../../../services/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../services/notifications.service';
import { AddressformComponent } from '../addressform/addressform.component';

@Component({
  selector: 'app-customerform',
  templateUrl: './customerform.component.html',
  styleUrls: ['./customerform.component.css']
})
export class CustomerformComponent implements OnInit {

  id: string;
  customerForm: FormGroup;
  customer: Customer;
  valid: boolean;
  addresses: Array<Address> = [];

  public customerAttributes:any = {
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl(''),
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _cS: CustomerService,
    private _nS: NotificationsService,
    private dialog: MatDialog,
  ) {
    this.customerForm = new FormGroup(this.customerAttributes);
    this.valid = true;
    //se obtiene el id de la ruta
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      //se busca el id en el servicio y se carga el objeto para luego cargar su informacion
      if(this.id){
        this.loadData(true);
      }
    });
  }

  ngOnInit() {

  }

  loadData(setValues:boolean = false){
    if(!this.id){ return false; }

    this._cS.show(this.id).subscribe( (resp:any) => {
      this.customer = resp.data;
      this.addresses = this.customer.addresses;
      if(setValues){ this.setFields(); }
    });
    return true;
  }

  setFields(){
    this.customerForm.patchValue({
      firstname: this.customer.firstname,
      lastname: this.customer.lastname,
      phone: this.customer.phone ,
      email: this.customer.email,
   });
  }

  //customer

  saveCustomer(){
    if(this.customerForm.invalid){
      this._nS.show('Faltan Campos', 'Tiene que llenar todos los campos primero', 'error');
      this.valid = false;
      return;
    }

    let customer: Customer = {
      firstname: this.customerForm.get('firstname').value,
      lastname: this.customerForm.get('lastname').value,
      phone: this.customerForm.get('phone').value,
      email: this.customerForm.get('email').value,
    };

    if(this.id){
      this._cS.update(this.customer.id, customer).subscribe( (resp:any) => {
        this._nS.show('Cliente Actualizado', resp.message, 'success');
        this.loadData();
      });
    } else {
      this._cS.create(customer, this.addresses).subscribe( (resp:any) => {
        this._nS.show('Cliente Creado', resp.message, 'success');
        this.router.navigate(['/customers']);
      });
    }
  }

  //addresses

  createAddress(){
    let dialogRef = this.dialog.open(AddressformComponent, {
      width: '350px',
      data: { id: this.customer ? this.customer.id : null, address: null }
    });

    dialogRef.afterClosed().subscribe( (resp:any) => {
      if(resp){
        resp.id = this.addresses.length+1;
        this.addresses.push(resp);
      }else{
        this.loadData();
      }
    });
  }

  editAddress(event, addr:Address){
    let dialogRef = this.dialog.open(AddressformComponent, {
      width: '350px',
      data: { id: this.customer.id, address: addr }
    });

    dialogRef.afterClosed().subscribe( (resp:any) => {
      this.loadData();
    });

    event.stopPropagation();
  }

  deleteAddress(event, addr:Address){
    if(this.customer){
      this._cS.removeAddr(addr.id).subscribe( (resp:any) => {
        this.loadData();
        this._nS.show('Direccion eliminada', resp.message, 'success');
      });
    }else{
      this.addresses = this.addresses.filter( obj => obj.id !== addr.id );
    }
  }
}
