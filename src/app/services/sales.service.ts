import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from './../core/constants';
import { BaseService } from './base.service';
import { NotificationsService } from './notifications.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { InvoiceProduct, Invoice } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class SalesService  extends BaseService {
  public url = {
    list: `${ API_URL }/invoices`,
    show_delete_update: `${ API_URL }/invoices/:id`,
    new: `${ API_URL }/addresses/:id/invoices`,
  }

  public statusList: Array<string> = [
    'En proceso',
    'Aprobada',
    'Rechazada',
    'Enviada',
    'Cancelada',
    'Other'
  ];

  constructor(
    public http:HttpClient,
    public _nS: NotificationsService
  ) { super(http); }

  //rutas de facturas
  public list = (nopaginate = false) => {
    let listurl:string = this.url.list;
    listurl = nopaginate ? listurl+"?nopagination" : listurl;
    return this.get(listurl);
  }

  public create = (id: string, body: Invoice, products: Array<InvoiceProduct>) => {
    let formData = new FormData();

    Object.keys(body).forEach(function(b){
      formData.append(`invoice[${b}]`, body[b]);
    });

    products.forEach( (prod, index) => {
      formData.append(`products[${index}][product_id]`, prod.product_id);
      formData.append(`products[${index}][amount]`, prod.amount.toString());
    });

    return this.post(this.url.new.replace(':id', id), formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public show = (id: string) => {
    return this.get(this.url.show_delete_update.replace(':id', id));
  }

  public update = (id: string, body: Invoice) => {
    let formData = new HttpParams();

    Object.keys(body).forEach(function(b){
      formData = formData.set(`invoice[${b}]`, body[b]);
    });

    return this.put(this.url.show_delete_update.replace(':id', id), formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public remove = (id: string) => {
    return this.delete(this.url.show_delete_update.replace(':id', id))
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }
}
