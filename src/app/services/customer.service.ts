import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from './../core/constants';
import { BaseService } from './base.service';
import { NotificationsService } from './notifications.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Customer, Address } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseService {
  public url = {
    main: `${ API_URL }/customers`,
    show_delete_update: `${ API_URL }/customers/:id`,

    newaddress: `${ API_URL }/customer/:id/addresses`,
    update_remove_address: `${ API_URL }/addresses/:id`,
  }

  constructor(
    public http:HttpClient,
    public _nS: NotificationsService
  ) { super(http); }

  //rutas de clientes
  public list = (nopaginate = false) => {
    let listurl:string = this.url.main;
    listurl = nopaginate ? listurl+"?nopagination" : listurl;
    return this.get(listurl);
  }

  public create = (body: Customer, addresses: Array<Address>) => {
    let formData = new FormData();

    Object.keys(body).forEach(function(b){
      formData.append(`customer[${b}]`, body[b]);
    });

    addresses.forEach( (addr, index) => {
      formData.append(`addresses[${index}][address]`, addr.address);
      formData.append(`addresses[${index}][description]`, addr.description);
      formData.append(`addresses[${index}][country]`, addr.country);
      formData.append(`addresses[${index}][state]`, addr.state);
      formData.append(`addresses[${index}][city]`, addr.city);
    });

    return this.post(this.url.main, formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public show = (id: string) => {
    return this.get(this.url.show_delete_update.replace(':id', id));
  }

  public update = (id: string, body: Customer) => {
    let formData = new HttpParams();

    Object.keys(body).forEach(function(b){
      formData = formData.set(`customer[${b}]`, body[b]);
    });

    return this.put(this.url.show_delete_update.replace(':id', id), formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public remove = (id: string) => {
    return this.delete(this.url.show_delete_update.replace(':id', id))
    .pipe(catchError(err => {
      if(err.status == 409){
        this._nS.show('Conflicto', 'No se pudo borrar el usuario ya que tiene informacion asociada', 'error');
      }else{
        this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      }
      return throwError(err);
    }));
  }

  //rutas de direcciones
  public createAddr = (id: string, body: Address) => {
    let formData = new FormData();

    Object.keys(body).forEach(function(b){
      formData.append(`address[${b}]`, body[b]);
    });

    return this.post(this.url.newaddress.replace(':id', id), formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public updateAddr = (id: string, body: Address) => {
    let formData = new HttpParams();

    Object.keys(body).forEach(function(b){
      formData = formData.set(`address[${b}]`, body[b]);
    });

    return this.put(this.url.update_remove_address.replace(':id', id), formData)
    .pipe(catchError(err => {
      this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      return throwError(err);
    }));
  }

  public removeAddr = (id: string) => {
    return this.delete(this.url.update_remove_address.replace(':id', id))
    .pipe(catchError(err => {
      if(err.status == 409){
        this._nS.show('Conflicto', 'No se pudo borrar la direccion ya que esta asociada a una factura', 'error');
      }else{
        this._nS.show('Algo no salio Bien', 'Por favor intente luego', 'error');
      }
      return throwError(err);
    }));
  }
}
