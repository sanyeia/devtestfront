import { API_URL } from './../core/constants';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from './notifications.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService {
  public url = {
    list: `${ API_URL }/products`,
  }

  constructor(
    public http:HttpClient,
    public _nS: NotificationsService
  ) { super(http); }

  //rutas de clientes
  public list = (nopaginate = false) => {
    let listurl:string = this.url.list;
    listurl = nopaginate ? listurl+"?nopagination" : listurl;
    return this.get(listurl);
  }
}
