import { NotificationsService } from './notifications.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from '../core/constants';
import { BaseService } from './base.service';
import { User } from '../interfaces/interfaces';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  public url = {
    login: `${ API_URL }/login`,
    logout: `${ API_URL }/logout`,
    create_user: `${ API_URL }/register`,
  }

  constructor(
    public http:HttpClient,
    public _nS: NotificationsService
  ) { super(http); }

  public logout = () => {
    this.get(this.url.logout);
    return this.clear_storage();
  }

  //user routes
  public login = (body) => {
    let formData = new HttpParams();
    Object.keys(body).forEach(function(b){
      formData = formData.set(b, body[b]);
    });

    return this.post(this.url.login, formData, false).pipe(
      map((resp: any) => {
        resp = resp.data;
        this.token = resp.token;
        this.user = resp.user;
        return resp.data;
      }),
      catchError(err => {
        if(err.status == 401){
          this._nS.show('Credenciales invalidas', '', 'error');
        }
        return throwError(err);
      })
    );
  }

  public create = (body: User) => {
    let formData = new FormData();
    Object.keys(body).forEach(function(b){
      formData.append(`user[${b}]`, body[b]);
    });

    return this.post(this.url.create_user, formData, false).pipe(
      map((resp: any) => {
        resp = resp.data;
        this.token = resp.token;
        this.user = resp.user;
        this._nS.show('Usuario Creado con exito', 'Ahora puede ingresar', 'success');
        return resp.data;
      }),
      catchError(err => {
        if(err.status == 409){
          this._nS.show('Usuario ya registrado', '', 'error');
        }
        return throwError(err);
      })
    );
  }

}
