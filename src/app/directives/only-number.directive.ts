import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: 'input[OnlyNumber]'
})
export class OnlyNumberDirective {

  constructor(private _el: ElementRef) { }

  @HostListener('input', ['$event']) onInputChange(event) {
    //se obtiene el valor ingresado
    let input = this._el.nativeElement.value;
    //se filtra usando un regex que solo acepta numeros
    this._el.nativeElement.value = input.replace(/[^0-9]*/g, '');
    //si se filtró se detiene la propagacion del evento
    if ( input !== this._el.nativeElement.value) { event.stopPropagation();}
  }
}
